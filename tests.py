#! python
import unittest
import json
from datetime import datetime
from app import create_app, db
from config import Config


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client
        self.car = {
            "car_make": "Иванов Денис Викторович",
            "car_phone": "8960343433",
            "car_serial_number": 131414382,
            "car_name": "HONDA"
        }
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_build_create(self):
        res = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        self.assertIn(self.car['car_phone'], str(res.data))

    def test_get_all_cars(self):
        res = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/cars', headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)
        self.assertIn(self.car['car_phone'], str(res.data))

    def test_get_car_by_id(self):
        rv = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'","\""))
        result = self.client().get('/api/cars/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)
        self.assertIn(self.car['car_phone'], str(result.data))

    def test_car_can_be_edited(self):
        rv = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        car_phone_old = self.car['car_phone']
        self.car['car_phone'] = 'Vezel'

        res = self.client().put('/api/cars/{}'.format(result_in_json['id']), data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 200)

        result = self.client().get('/api/cars/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.car['car_phone'], str(result.data))
        self.assertNotIn(car_phone_old, str(result.data))

    def test_build_deletion(self):
        rv = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        result = self.client().delete('/api/cars/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 200)

        result = self.client().get('/api/cars/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertEqual(result.status_code, 404)

    def test_put_empty_object(self):
        rv = self.client().post('/api/cars', data=json.dumps(self.car), headers={'Content-Type': 'application/json'})
        self.assertEqual(rv.status_code, 201)
        result_in_json = json.loads(rv.data.decode('utf-8').replace("'", "\""))

        build_upd = {}

        res = self.client().put('/api/cars/{}'.format(result_in_json['id']), data=json.dumps(build_upd), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Build should contain something', str(res.data))

        result = self.client().get('/api/cars/{}'.format(result_in_json['id']), headers={'Content-Type': 'application/json'})
        self.assertIn(self.car['car_phone'], str(result.data))

    def test_post_empty_object(self):
        res = self.client().post('/api/cars', data=json.dumps({}), headers={'Content-Type': 'application/json'})
        self.assertEqual(res.status_code, 400)
        self.assertIn('Car should contain something', str(res.data))


if __name__ == "__main__":
    unittest.main()
